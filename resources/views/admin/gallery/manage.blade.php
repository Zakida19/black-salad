@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="card-title">
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </div>
                </div>
            </div>

            <div class="card-tools">
                <span class="badge">
                    <a href="{{ route('gallery.add') }}" type="button" class="btn btn-info btn-sm">Add</a>
                </span>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th style="width: 40px" class="text-center">Manage</th>
                </tr>
              </thead>
              <tbody>
                @foreach($galleries as $no => $gallery)
                <tr>
                    <td>Gallery {{ $no + 1 }}</td>
                    <td>
                        <img src="{{ asset('/api/images/galleries/'.$gallery->image) }}" alt="" style="width: 170px;" height="120px" class="img-rounded">
                    </td>

                    <td class="d-flex" style="column-gap: 5px">
                        <a href="{{ route('gallery.edit', $gallery) }}" type="button" class="btn btn-success btn-sm">Edit</a>
                        <a href="" type="button" class="btn btn-info btn-sm">View</a>
                        <form action="{{ route('gallery.delete', $gallery) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
    </div>
</div>
@endsection
