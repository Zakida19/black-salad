@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-seccond">
        <div class="card-header">
          <h3 class="card-title">Add Gallery</h3>
        </div>
        <form method="post" action="{{ route('gallery.update', $gallery) }}" enctype="multipart/form-data">
        @csrf
        @method('put')
          <div class="card-body">
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" name="title" id="title" placeholder="Gallery Title" value="{{ old('title', $gallery->title) }}">
              @error('title')
                <div class="mt-2 text-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="image">Gallery Image</label>
                <div class="mb-3">
                    <img src="{{ asset('/api/images/galleries/'.$gallery->image) }}" alt="" style="width: 170px;" height="120px" class="img-rounded">
                </div>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="image" id="image">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
                @error('image')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-info btn-sm">Update</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection
