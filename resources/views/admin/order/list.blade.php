@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="card-title">
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table">
              <thead>
                <tr>
                  <th style="width: 10px">Order ID</th>
                  <th>Customer</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Total</th>
                  <th>Date</th>
                  <th>Dikirim</th>
                  <th style="width: 40px">Manage</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>#{{ $order->id }}</td>
                    <td>{{ $order->name }}</td>
                    <td>{{ Str::limit($order->address, 25) }}</td>
                    <td>{{ $order->phone }}</td>
                    <td>Rp.{{ $order->total }}</td>
                    <td>{{ $order->created_at->format('d/m/Y')}}</td>
                    <td>
                        @if ($order->shipped === 1)
                            <span class="badge bg-success">Dikirim</span>
                        @else
                            <span class="badge bg-warning">Proses</span>
                        @endif
                    </td>

                    <td class="d-flex" style="column-gap: 5px">
                        <a href="{{ route('order.edit', $order) }}" type="button" class="btn btn-success btn-sm">Shipped</a>
                        <a href="{{ route('order.detail', $order) }}" type="button" class="btn btn-warning btn-sm">View</a>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          {{-- <div class="card-body p-0">
            <table class="table">
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>Item</th>
                  <th>Quantity</th>
                  <th style="width: 40px">Manage</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orderItem as $order)
                <tr>
                    <td>{{ $order->order_id }}</td>
                    <td>{{ $order->item_id }}</td>
                    <td>{{ $order->quantity }}</td>
                    <td class="d-flex" style="column-gap: 5px">
                        <a href="" type="button" class="btn btn-info btn-sm">Edit</a>
                        <form action="" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div> --}}

        </div>
    </div>
</div>
@endsection
