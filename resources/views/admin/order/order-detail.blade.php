@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row mb-3">
              <div class="col-12">
                <h4>
                  <i class="fas fa-globe"></i> BlackFood, Inc.
                <small class="float-right">Date: {{ $order->created_at->format('d/m/Y') }}</small>
                </h4>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <!-- /.col -->
              <div class="col-sm-6 invoice-col">
                <address>
                  <strong>{{ $order->name }}</strong>
                  <br>
                  {{ $order->address }}
                  <br>
                  Phone: {{ $order->phone }}<br>
                  Email: {{ $order->email }}
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-6 invoice-col">
                <b>User Id #{{ $order->user_id }}</b><br>
                <br>
                <b>Order ID:</b> {{ $order->id }}<br>
                <b>Payment Gate:</b> {{ $order->payment_gateway }}<br>
                <b>Satatus:</b> @if ($order->shipped == '1')
                <span class="badge bg-success">Dikirim</span>

                @else
                <span class="badge bg-danger">Prosess</span>
                @endif
              </div>
              <!-- /.col -->
            </div>
        </div>

        <div class="card">
            <div class="card-header">
              <div class="card-title">
                <h4>
                     Order List
                </h4>
                  {{-- <div class="card-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                        <div class="input-group-append">
                          <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                      </div>
                  </div> --}}
              </div>
            </div>
            <!-- /.card-header -->

            <div class="card-body p-0">
              <table class="table">
                <thead>
                  <tr>
                    <th>Item</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th style="width: 40px">Manage</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($order->itemOrders as $order)
                  <tr>
                      <td>{{ $order->item->name }}</td>
                      <td>
                        {{-- <img src="{{ asset('/api/images/items/'.$item->image) }}" alt="Product Image"> --}}
                        <img src="{{ asset('/api/images/items/'.$order->item->image) }}" alt="" style="width: 150px;" height="100px" class="img-rounded">
                    </td>
                      <td>{{ $order->item->price }}</td>
                      <td>
                        <b>{{ $order->quantity }}</b>
                      </td>
                      <td class="d-flex" style="column-gap: 5px">
                          <a href="" type="button" class="btn btn-info btn-sm">Edit</a>
                          <form action="" method="post">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                          </form>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
    </div>
</div>
@endsection
