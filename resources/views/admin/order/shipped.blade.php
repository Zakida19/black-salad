@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-seccond">
        <div class="card-header">
          <h3 class="card-title">Add Gallery</h3>
        </div>
        <form method="post" action="{{  route('order.shipped', $order) }}" enctype="multipart/form-data">
        @csrf
        @method('put')
          <div class="card-body">
            <div class="form-group">
                <div class="form-check">
                <input class="form-check-input" name="shipped" type="checkbox" value="1" {{ $order->shipped == 1 ? 'checked': '' }}>
                <label class="form-check-label">Dikirim</label>
                </div>
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-info btn-sm">Update</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection
