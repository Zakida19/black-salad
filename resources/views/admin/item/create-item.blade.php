@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-seccond">
        <div class="card-header">
          <h3 class="card-title">Add Item</h3>
        </div>
        <form method="post" action="{{ route('item.store') }}" enctype="multipart/form-data">
        @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="name">Product Item</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Item Product" value="{{ old('name') }}">
              @error('name')
                <div class="mt-2 text-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="image">Item Image</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="image" id="image">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
                @error('image')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
              </div>
            <div class="form-group">
                <label for="name">Price</label>
                <input type="number" class="form-control" name="price" id="price" placeholder="Price" value="{{ old('price') }}">
                @error('price')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Quantity Item</label>
                <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity Item" value="{{ old('quantity') }}">
                @error('quantity')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Description</label>
                <textarea class="form-control" rows="3" name="description" id="description" placeholder="Description ..."></textarea>
                @error('description')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
            {{-- <div class="form-group">
                <label>Sub Category</label>
                <select class="form-control" name="subcategory" id="subcategory" style="width: 100%;">
                  @foreach($subcategories as $categories)
                  <option value="{{ $categories->id }}">{{ $categories->name }}</option>
                  @endforeach
                </select>
                @error('subcategory')
                    <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div> --}}
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-info btn-sm">Submit</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection
