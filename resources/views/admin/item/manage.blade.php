@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('layouts.message')
        <div class="card">
          <div class="card-header">
            <div class="card-title">
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </div>
                </div>
            </div>

            <div class="card-tools">
                <span class="badge">
                    <a href="{{ route('item.create') }}" type="button" class="btn btn-info btn-sm">Create</a>
                </span>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Item Product</th>
                  <th>Image</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Description</th>
                  <th>Stock Item</th>
                  <th style="width: 40px">Manage</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        {{ Str::limit($item->image, 10) }}
                    </td>
                    <td>Rp.{{ $item->price }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ Str::limit($item->description, 30) }}</td>
                    <td>
                        @if ($item->isAvailable == '1')
                        <span class="badge bg-success">Tersedia</span>

                        @else
                        <span class="badge bg-danger">Tidak Tersedia</span>
                        @endif
                    </td>
                    <td class="d-flex" style="column-gap: 5px">
                        <a href="{{ route('item.detail', $item) }}" type="button" class="btn btn-success btn-sm">Detail</a>
                        <a href="{{ route('item.edit', $item) }}" type="button" class="btn btn-info btn-sm">Edit</a>
                        <form action="{{ route('item.delete', $item) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection
