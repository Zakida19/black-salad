@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-seccond">
        <div class="card-header">
          <h3 class="card-title">Edit Item</h3>
        </div>
        <form method="post" action="{{ route('item.update', $item) }}" enctype="multipart/form-data">
        @csrf
        @method('put')
          <div class="card-body">
            <div class="form-group">
              <label for="name">Product Item</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Item Product" value="{{ old('name', $item->name) }}">
              @error('name')
                <div class="mt-2 text-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="image">Item Image</label>
                <div class="mb-3">
                    <img src="{{ asset('/api/images/items/'.$item->image) }}" alt="" style="width: 170px;" height="120px" class="img-rounded">
                </div>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="image" id="image">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                  </div>
                  <div class="input-group-append">
                    <span class="input-group-text">Upload</span>
                  </div>
                </div>
                @error('image')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
              </div>
            <div class="form-group">
                <label for="name">Price</label>
                <input type="number" class="form-control" name="price" id="price" placeholder="Price" value="{{ old('name', $item->price) }}">
                @error('price')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Quantity Item</label>
                <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity Item" value="{{ old('quantity', $item->quantity) }}">
                @error('quantity')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <div class="form-check">
                <input class="form-check-input" name="isAvailable" type="checkbox" value="1" {{ $item->isAvailable == 1 ? 'checked': '' }}>
                <label class="form-check-label">Tersedia</label>
                </div>
            </div>
            <div class="form-group">
                <label for="name">Description</label>
                <textarea class="form-control" rows="3" name="description" id="description" placeholder="description ...">{{ $item->description }}</textarea>
                @error('kota')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-info btn-sm">Update</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection
