@extends('layouts.master-admin')

@section('content')
<div class="card card-solid">
    <div class="card-body">
      <div class="row">
        <div class="col-12 col-sm-6">
          <h3 class="d-inline-block d-sm-none">{{ $item->name }}</h3>
          <div class="col-12">
            <img src="{{ asset('/api/images/items/'.$item->image) }}" class="product-image" alt="Product Image">
          </div>
          <div class="col-12 product-image-thumbs">
            @for ($image = 0; $image <= 3; $image++)
            <div class="product-image-thumb active">
                <img src="{{ asset('/api/images/items/'.$item->image) }}" alt="Product Image">
            </div>
            @endfor
          </div>
        </div>
        <div class="col-12 col-sm-6">
          <h3 class="my-3">{{ $item->name }}</h3>
          <p>{{ $item->description }}</p>

          <hr>
          <h4 class="my-3">Stock</h4>
                @if ($item->isAvailable == '1')
                <span class="text-md badge bg-success">
                    Tersedia
                </span>

                @else
                <span class="text-md badge bg-danger">
                    Tidak Tersedia
                </span>
                @endif

          <h4 class="mt-3">Quantity</h4>
          <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-default text-center">
              <input type="radio" name="color_option" id="color_option_b1" autocomplete="off">
              <span class="text-md">{{ $item->quantity }}</span>
              <br>
              Items
            </label>
          </div>

          <div class="bg-gray py-2 px-3 mt-4">
            <h2 class="mb-0">
              Rp.{{ $item->price }}
            </h2>
            <h4 class="mt-0">
              <small>Ex Tax: Rp.{{ $item->price * 1.2 }} </small>
            </h4>
          </div>

          <div class="mt-4 product-share">
            <a href="#" class="text-blue">
              <i class="fab fa-facebook-square fa-2x"></i>
            </a>
            <a href="#" class="text-info">
              <i class="fab fa-twitter-square fa-2x"></i>
            </a>
            <a href="#" class="text-red">
              <i class="fas fa-envelope-square fa-2x"></i>
            </a>
            <a href="#" class="text-yellow">
              <i class="fas fa-rss-square fa-2x"></i>
            </a>
          </div>

        </div>
      </div>
      <div class="row mt-4">
        <nav class="w-100">
          <div class="nav nav-tabs" id="product-tab" role="tablist">
            <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">Description</a>
            <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#product-comments" role="tab" aria-controls="product-comments" aria-selected="false">Comments</a>
            <a class="nav-item nav-link" id="product-rating-tab" data-toggle="tab" href="#product-rating" role="tab" aria-controls="product-rating" aria-selected="false">Rating</a>
          </div>
        </nav>
        <div class="tab-content p-3" id="nav-tabContent">
            <div class="tab-pane fade show active" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab"> {{ $item->description }}
            </div>
            <div class="tab-pane fade" id="product-comments" role="tabpanel" aria-labelledby="product-comments-tab">
            </div>
            <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab">
            </div>
        </div>
      </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection
