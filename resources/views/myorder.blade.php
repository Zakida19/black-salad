@extends('layouts.frontend')

@section('content')
<section class="page-title text-center bg-img overlay" style="background-image: url(img/page_title/about_us_title_bg.jpg)">
    <div class="container relative clearfix">
      <div class="title-holder">
        <div class="title-text">
          <h1 class="uppercase">My Order</h1>
          <ol class="breadcrumb">
            <li>
              <a href="index.html">Home</a>
            </li>
            <li>
              <a href="index.html">My Order</a>
            </li>
          </ol>
        </div>
      </div>
    </div>
</section>


    <!-- Intro -->
    <section class="section-wrap intro pb-0">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 mb-50">
            <h2 class="intro-heading">about our shop</h2>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($myOrder as $order)
                            <tr>
                                <td>#{{ $order->id }}</td>
                                <td>{{ $order->created_at->format('d/m/Y')}}</td>
                                <td>
                                    @if ($order->shipped === 1)
                                        <span class="badge bg-success">Dikirim</span>
                                    @else
                                        <span class="badge bg-warning">Proses</span>
                                    @endif
                                </td>
                                <td>Rp.{{ $order->total }}</td>
                                <td><a href="{{ route('myorder-detail', $order) }}" class="btn-small d-block">View</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <span class="result">{{ $myOrder->count() }}</span>
            <p>Years on Global Market.</p>
            <span class="result">45</span>
            <p>Partners are Working With Us.</p>
          </div>
        </div>
        <hr class="mb-0">
      </div>
    </section> <!-- end intro -->

    <div id="back-to-top">
      <a href="#top"><i class="fa fa-angle-up"></i></a>
    </div>
@endsection
