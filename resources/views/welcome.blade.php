@extends('layouts.frontend')

@section('content')
<!-- Hero Slider -->
<section class="hero-wrap text-center relative pb-30">
    <div id="owl-hero" class="owl-carousel owl-theme light-arrows slider-animated">
        @foreach($galleries as $gallery)
        <div class="hero-slide overlay" style="background-image:url({{ asset('/api/images/galleries/'.$gallery->image) }})">
            <div class="container">
                <div class="hero-holder">
                <div class="hero-message">
                    <h1 class="hero-title nocaps">Great Fashion 2017</h1>
                    <h2 class="hero-subtitle lines">New Arrivals Collection</h2>
                    <div class="buttons-holder">
                    <a href="#" class="btn btn-lg btn-transparent"><span>Shop Now</span></a>
                    </div>
                </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section> <!-- end hero slider -->


<!-- Trendy Products -->
<section class="section-wrap-sm new-arrivals pb-50">
<div class="container">

    <div class="row heading-row">
        <div class="col-md-12 text-center">
            <span class="subheading">Hot items of this year</span>
            <h2 class="heading bottom-line">
            trendy products
            </h2>
        </div>
    </div>


    <div class="row items-grid">
        @foreach($items as $item)
        <div class="col-md-3 col-xs-6">
            <div class="product-item hover-trigger">
            <div class="product-img">
                <a href="shop-single.html">

                {{-- <img src="{{ asset('/api/images/galleries/'.$gallery->image) }}" alt=""> --}}
                <img src="{{ asset('/api/images/items/'.$item->image) }}" alt="Product Image">
                </a>
                <div class="product-label">
                    @if ( $item->isAvailable == '1' )
                    <span class="sale">Tersedia</span>
                    @else
                    <span class="sale">Tidak Tersedia</span>
                    Tidak Tersedia
                    @endif
                </div>
                <div class="hover-overlay">
                <div class="product-actions">
                    <a href="#" class="product-add-to-wishlist">
                    <i class="fa fa-heart"></i>
                    </a>
                </div>
                <div class="product-details valign">
                    <span class="category">
                    <a href="catalogue-grid.html">
                        @if ( $item->isAvailable == '1' )
                        Tersedia
                        @else
                        Tidak Tersedia
                        @endif
                    </a>
                    </span>
                    <h3 class="product-title">
                    <a href="shop-single.html">{{ $item->name }}</a>
                    </h3>
                    <span class="price">
                    <del>
                        <span>Rp.{{ $item->price * 1.5 }}</span>
                    </del>
                    <ins>
                        <span class="amount">Rp.{{ $item->price }}</span>
                    </ins>
                    </span>
                    <div class="btn-quickview">
                        <a href="{{ route('cart.add', $item) }}" class="btn btn-md btn-color"aria-label="Add To Cart">
                            <span>Order</span>
                        </a>
                    {{-- <form id="cartSubmit" action="{{ route('cart.add') }}" method="post" >
                        @csrf
                        <input type="hidden" name="id" value="{{ $item->id }}">
                        <input type="hidden" name="name" value="{{ $item->name }}">
                        <input type="hidden" name="price" value="{{ $item->price }}">
                        {{ $subTotal = $item->price * $item->quantity }}
                        {{ Session::put('sum', $subTotal); }}
                        <button type="submit" class="btn btn-md btn-color"aria-label="Add To Cart">
                            <span>Add To Cart</span>
                        </button>
                    </form> --}}
                    {{-- <a href="#" class="btn btn-md btn-color">
                    <span>Quickview</span>
                    </a> --}}
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        @endforeach
    </div>

</div>
</section> <!-- end trendy products -->

<!-- Testimonials -->
<section class="section-wrap relative testimonials bg-parallax overlay" style="background-image:url(img/testimonials/testimonial_bg.jpg);">
    <div class="container relative">

        <div class="row heading-row mb-20">
        <div class="col-md-6 col-md-offset-3 text-center">
            <h2 class="heading white bottom-line">Happy Clients</h2>
        </div>
        </div>

        <div id="owl-testimonials" class="owl-carousel owl-theme text-center">

            <div class="item">
                <div class="testimonial">
                <p class="testimonial-text">I’am amazed, I should say thank you so much for your awesome template. Design is so good and neat, every detail has been taken care these team are realy amazing and talented! I will work only with this agency.</p>
                <span>Donald Trump / CEO of Trump organization</span>
                </div>
            </div>
        </div>
    </div>

</section> <!-- end testimonials -->


{{-- <!-- Product Widgets List -->
<section class="section-wrap products-list">
<div class="container">
    <div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12 mb-40 products-widget">
        <h3 class="widget-title bottom-line full-grey">Special Offer</h3>
        <ul class="product-list-widget">
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_1.jpg" alt="">
            <span class="product-title">Skinny Dress</span>
            </a>
            <span class="price">
            <del>
                <span>$388.00</span>
            </del>
            <ins>
                <span class="amount">$159.99</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_2.jpg" alt="">
            <span class="product-title">Mesh Brown Sandal</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$190.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_3.jpg" alt="">
            <span class="product-title">Tribal Grey Blazer</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$330.00</span>
            </ins>
            </span>
        </li>
        </ul>
    </div> <!-- end special offer -->

    <div class="col-md-3 col-sm-6 col-xs-12 mb-40 products-widget">
        <h3 class="widget-title bottom-line full-grey">Bestsellers</h3>
        <ul class="product-list-widget">
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_9.jpg" alt="">
            <span class="product-title">Camo Belt</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$33.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_10.jpg" alt="">
            <span class="product-title">Heathered Scarf</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$180.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_11.jpg" alt="">
            <span class="product-title">Mattle Brown Bag</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$150.00</span>
            </ins>
            </span>
        </li>
        </ul>
    </div> <!-- end bestsellers -->

    <div class="col-md-3 col-sm-6 col-xs-12 mb-40 products-widget">
        <h3 class="widget-title bottom-line full-grey">Accessories</h3>
        <ul class="product-list-widget">
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_5.jpg" alt="">
            <span class="product-title">Lola May Crop Blazer</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$42.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_7.jpg" alt="">
            <span class="product-title">Watch</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$280.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_8.jpg" alt="">
            <span class="product-title">Jersey Stylish</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$289.00</span>
            </ins>
            </span>
        </li>
        </ul>
    </div> <!-- end top rated -->

    <div class="col-md-3 col-sm-6 col-xs-12 products-widget last">
        <h3 class="widget-title bottom-line full-grey">top rated</h3>
        <ul class="product-list-widget">
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_4.jpg" alt="">
            <span class="product-title">Sweater w/ Colar</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$299.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_6.jpg" alt="">
            <span class="product-title">Faux Suits</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$233.00</span>
            </ins>
            </span>
        </li>
        <li class="clearfix">
            <a href="shop-single-product.html">
            <img src="img/shop/shop_item_12.jpg" alt="">
            <span class="product-title">Sport T-Shirt</span>
            </a>
            <span class="price">
            <ins>
                <span class="amount">$230.00</span>
            </ins>
            </span>
        </li>
        </ul>
    </div> <!-- end sales -->

    </div> <!-- end row -->
</div>
</section> <!-- end product widgets --> --}}


<!-- Newsletter -->
<section class="newsletter" id="subscribe">
<div class="container">
    <div class="row">
    <div class="col-sm-12 text-center">
        <h4>Get the latest updates</h4>
        <form class="relative newsletter-form">
        <input type="email" class="newsletter-input" placeholder="Enter your email">
        <input type="submit" class="btn btn-lg btn-dark newsletter-submit" value="Subscribe">
        </form>
    </div>
    </div>
</div>
</section>
@endsection
