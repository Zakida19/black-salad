@extends('layouts.frontend')

@section('content')
<section class="section-wrap checkout pb-70">
    <div class="container relative">
      <div class="row">

        <div class="ecommerce col-xs-12">

          <div class="row mb-30">
            <div class="col-md-8">
              <p class="ecommerce-info">
                Returning Customer?
                <a href="#" class="showlogin">Click here to login</a>
              </p>
            </div>
          </div>

          <form name="" action="{{ route('checkout.order') }}" method="post" class="checkout ecommerce-checkout row">
            @csrf
            <div class="col-md-8" id="customer_details">
              <div>
                <h2 class="heading uppercase bottom-line full-grey mb-30">billing address</h2>

                <p class="form-row form-row-first validate-required ecommerce-invalid ecommerce-invalid-required-field" id="billing_first_name_field">
                  <label for="billing_first_name">Name
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder value name="name" id="billing_first_name">
                </p>

                <p class="form-row form-row-wide address-field validate-required ecommerce-invalid ecommerce-invalid-required-field" id="billing_address_1_field">
                  <label for="billing_address_1">Address
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text"  class="input-text" placeholder="Street address" value name="address" id="billing_address_1">
                </p>

                {{-- <p class="form-row form-row-wide address-field" id="billing_address_2_field">
                  <input type="text" class="input-text" placeholder="Apartment, suite, unit etc. (optional)" value name="address" id="address">
                </p> --}}

                <p class="form-row form-row-wide address-field validate-required" id="city" data-o_class="form-row form-row-wide address-field validate-required">
                  <label for="billing_city">City
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder="City" value name="city" id="city">
                </p>

                <p class="form-row form-row-wide address-field validate-required" id="city" data-o_class="form-row form-row-wide address-field validate-required">
                    <label for="billing_city">Provinsi
                      <abbr class="required" title="required">*</abbr>
                    </label>
                    <input type="text" class="input-text" placeholder="Provinsi" value name="province" id="province">
                  </p>

                {{-- <p class="form-row form-row-first validate-required validate-email" id="billing_email_field">
                  <label for="billing_email">Email Address
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder value name="email" id="email">
                </p> --}}

                <p class="form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                  <label for="billing_phone">Phone
                    <abbr class="required" title="required">*</abbr>
                  </label>
                  <input type="text" class="input-text" placeholder value name="phone" id="phone">
                </p>

                <div class="clear"></div>

              </div>

              {{-- <p class="form-row form-row-wide create-account">
                <input type="checkbox" class="input-checkbox" id="createaccount" name="createaccount" value="1">
                <label for="createaccount" class="checkbox">Create an account?</label>
              </p>

              <div class="clear"></div>

              <div>
                <div class="ecommerce-shipping-fields">
                  <input type="checkbox" id="ship-to-different-address-checkbox" class="input-checkbox" name="ship_to_different_address" value="1">
                  <label for="ship-to-different-address-checkbox" class="checkbox">Ship to a different address</label>
                </div>
                <p class="form-row notes ecommerce-validated" id="order_comments_field">
                  <label for="order_comments">Order Notes</label>
                  <textarea name="order_comments" class="input-text" id="order_comments" placeholder="Notes about your order, e.g. special notes for delivery." rows="2" cols="6"></textarea>
                </p>
              </div> --}}

              <div class="clear"></div>

            </div> <!-- end col -->

            <!-- Your Order -->
            <div class="col-md-4">
              <div class="order-review-wrap ecommerce-checkout-review-order" id="order_review">
                <h2 class="heading uppercase bottom-line full-grey">Your Order</h2>
                <table class="table shop_table ecommerce-checkout-review-order-table">
                  <tbody>
                    @foreach($cartsCollect as $item)
                    {{-- @php
                        $subtotal = $item['price'] * $item['quantity'];
                        $total += $subtotal;
                    @endphp --}}
                    <tr>
                      <th>{{ $item['name'] }}<span class="count"> x {{ $item['quantity'] }}</span></th>
                      <td>
                        <span class="amount">Rp.{{ $item->getPriceSum() }}</span>
                      </td>
                    </tr>
                    @endforeach
                    <tr class="cart-subtotal">
                      <th>Cart Subtotal</th>
                      <td>
                        <span class="amount">Rp.{{ $summary['subTotal'] }}</span>
                      </td>
                    </tr>
                    <tr class="shipping">
                      <th><strong>Shipping</strong></th>
                      <td>
                        <span>Free Shipping</span>
                      </td>
                    </tr>
                    <tr class="order-total">
                      <th><strong>Order Total</strong></th>
                      <td>
                        <strong><span class="amount">Rp.{{ $summary['total'] }}</span></strong>
                      </td>
                      {{ Session::put('sum', $summary['total']) }}
                    </tr>
                  </tbody>
                </table>

                <div id="payment" class="ecommerce-checkout-payment">
                  <h2 class="heading uppercase bottom-line full-grey">Payment Method</h2>
                  <ul class="payment_methods methods">

                    <li class="payment_method_cheque">
                        <input id="cash" type="radio" class="input-radio" name="payment_method" value="cash">
                        <label for="cash">Cash On Delievery</label>
                        <div class="payment_box">
                          <p>Metode Bayar ditempat</p>
                        </div>
                      </li>

                    <li class="payment_method_bacs">
                      <input id="bank" type="radio" class="input-radio" name="payment_method" value="bank">
                      <label for="bank">Direct Bank Transfer</label>
                      <div class="payment_box">
                        <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order wont be shipped until the funds have cleared in our account.</p>
                      </div>
                    </li>

                    {{-- <li class="payment_method_paypal">
                      <input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="paypal">
                      <label for="payment_method_paypal">Paypal</label>
                      <img src="img/shop/paypal.png" alt="">
                      <div class="payment_box payment_method_paypal">
                        <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
                      </div>
                    </li> --}}

                  </ul>
                  <div class="form-row place-order">
                    <input type="submit" class="btn btn-lg btn-dark" id="place_order" value="Place order">
                  </div>
                </div>
              </div>
            </div> <!-- end order review -->
          </form>

        </div> <!-- end ecommerce -->

      </div> <!-- end row -->
    </div> <!-- end container -->
</section>
@endsection
