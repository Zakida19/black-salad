<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\ItemController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// User Akses
Route::middleware('auth')->group(function() {
    // Carts
    Route::get('mycart', [CartController::class, 'mycart'])->name('mycart');

    Route::get('cart/{item}/add', [CartController::class, 'addToCart'])->name('cart.add');
    Route::get('cart/{rowId}/addQty', [CartController::class, 'increase_item'])->name('cart.addQuantity');
    Route::get('cart/{rowId}/minQty', [CartController::class, 'decrease_item'])->name('cart.minQuantity');
    Route::get('cart/{item}/remove', [CartController::class, 'remove_from_cart'])->name('cart.remove');
    Route::get('cart/reset', [CartController::class, 'cart_reset'])->name('cart.clear');

    // Checkout
    Route::get('checkout', [CheckoutController::class, 'index'])->name('checkout.page');
    Route::post('checkout/order', [CheckoutController::class, 'order'])->name('checkout.order');

    // Customer
    Route::get('myorder',[CustomerController::class, 'myorder'])->name('myorder');
    Route::get('myorder/{myOrder}/detail',[CustomerController::class, 'detail'])->name('myorder-detail');

});


// Admin Akses
Route::group(['prefix' => 'admin'], function() {

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::group(['prefix' => 'item' ,'as' => 'item.'], function() {
        Route::get('/', [ItemController::class, 'manage'])->name('manage');
        Route::get('/create', [ItemController::class, 'create'])->name('create');
        Route::post('/store', [ItemController::class, 'store'])->name('store');
        Route::get('/{item:slug}/edit', [ItemController::class, 'edit'])->name('edit');
        Route::put('{item:slug}/update', [ItemController::class, 'update'])->name('update');
        Route::delete('/{item:slug}/delete', [ItemController::class, 'delete'])->name('delete');
        Route::get('/{item:slug}/detail', [ItemController::class, 'detail'])->name('detail');
    });

    Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
        Route::get('/', [OrderController::class, 'manage'])->name('manage');
        Route::get('/{order}/detail', [OrderController::class, 'detail'])->name('detail');
        Route::get('/{order}/editShipped', [OrderController::class, 'edit_shipped'])->name('edit');
        Route::put('/{order}/shipped', [OrderController::class, 'shipped'])->name('shipped');
    });

    Route::group(['prefix' => 'gallery', 'as' => 'gallery.'], function() {
        Route::get('/manage', [GalleryController::class, 'manage'])->name('manage');
        Route::get('/add', [GalleryController::class, 'add'])->name('add');
        Route::post('/upload', [GalleryController::class, 'upload'])->name('upload');
        Route::get('/{gallery:slug}/edit', [GalleryController::class, 'edit'])->name('edit');
        Route::put('/{gallery:slug}/update', [GalleryController::class, 'update'])->name('update');
        Route::delete('/{gallery:slug}/delete', [GalleryController::class, 'delete'])->name('delete');
    });


});
