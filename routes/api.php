<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\ItemController;
use App\Http\Controllers\Api\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Items Api Route
Route::get('items', [ItemController::class, 'manage']);
Route::post('item/store', [ItemController::class, 'store']);
Route::get('item/{item}/detail', [ItemController::class, 'detail']);
Route::put('item/{item:slug}/update', [ItemController::class, 'update']);
Route::delete('item/{item:slug}/delete', [ItemController::class, 'delete']);

// Orders Api Route
Route::get('orders', [OrderController::class, 'list']);
Route::get('order/{order}/detail', [OrderController::class, 'detail']);
Route::delete('order/{order}/delete', [OrderController::class, 'delete']);

// Galleries Api Route
Route::get('galleries', [GalleryController::class, 'manage']);
Route::post('gallery/upload', [GalleryController::class, 'upload']);
Route::put('gallery/{gallery:slug}/update', [GalleryController::class, 'update']);
Route::delete('gallery/{gallery:slug}/delete', [GalleryController::class, 'delete']);



Route::middleware('auth:sanctum')->group(function() {
    // User
    Route::get('mycart', [CartController::class, 'mycart']);

    Route::get('cart/{item}/add', [CartController::class, 'addToCart']);
    Route::get('cart/{rowId}/increase_item', [CartController::class, 'increaseItem']);
    Route::get('cart/{rowId}/decrease_item', [CartController::class, 'decreaseItem']);
    Route::get('cart/{rowId}/remove_item', [CartController::class, 'removeItem']);

    Route::post('logout', [AuthController::class, 'logout']);
});
