<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GlobalCartsConfig
{
    public function __construct()
    {
    //    $this->middleware('auth');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $cartsUser = \Cart::session(auth()->user()->id);

        $carts = $cartsUser->getContent();
        $cartsTotal = $cartsUser->getTotal();

        view()->share(['carts' => $carts, 'cartsTotal' => $cartsTotal]);

        return $next($request);
    }
}
