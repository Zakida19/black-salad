<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendResponse($results, $message)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data' => $results,
        ];

        // if (!empty($results2)) {
        //     $response = [
        //         'success' => true,
        //         'message' => $message,
        //         'data' => $results,
        //         'collect' => $results2,
        //     ];
        // }
        // else {
        //     $response = [
        //         'success' => true,
        //         'message' => $message,
        //         'data' => $results,
        //     ];
        // }


        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error
        ];

        if (!empty($errorMessages)) {
            $response['date'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
