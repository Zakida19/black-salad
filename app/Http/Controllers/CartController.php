<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Darryldecode\Cart\Cart;

class CartController extends Controller
{
    public function mycart()
    {
        // $itemsCollect = Item::orderBy('created_at', 'DESC')->get();
        $userId = auth()->user()->id;

        // Data Cart yang dimiliki user yang sedang login
        $cartsUser = \Cart::session($userId);

        // Koleksi items cart user
        $cartsCollect = $cartsUser->getContent();

        // Memanggil item-item yang ada di cartCollect
        $items = $cartsCollect->sortBy(function ($cart) {
           return $cart->attributes->get('add_to_cart');
       });

        // Jika cartsCollect nya kosong
        if($cartsCollect->isEmpty()) {
           $cartsItem = [];
        }
        else {
            foreach ($items as $item) {
                $dataCart[] = [
                    'rowId' => $item->id,
                    'name' => $item->name,
                    'image' => $item->attributes->image,
                    'qty' => $item->quantity,
                    'pricesingle' => $item->price,
                    'price' => $item->getPriceSum(),
                ];
            }

            $cartsItem = collect($dataCart);
        }

        $subTotal = $cartsUser->getSubTotal();
        $total = $cartsUser->getTotal();

        $summary = [
            'total' => $total,
        ];

        return view('mycart', compact('cartsItem', 'summary'));
    }

    public function addToCart(Item $item)
    {
        $rowId = $item->id;
        $cartsUser = \Cart::session(auth()->user()->id);

        $cartsCollect = $cartsUser->getContent();
        $cartsItem = $cartsCollect->whereIn('id', $rowId);

        if ($cartsItem->isNotEmpty()) {
            $cartsUser->update($rowId, [
                'quantity' => [
                    'relative' => true,
                    'value' => 1,
                ]
            ]);
        }
        else {
            $cartsUser->add([
                'id' => $item->id,
                'name' => $item->name,
                'price' => $item->price,
                'quantity' => 1,
                'attributes' => [
                    'image' => $item->image,
                    'add_to_cart' => Carbon::now()
                ],
            ]);
        }
        return redirect()->route('mycart');
    }


    public function increase_item($rowId)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsUser->update($rowId, [
            'quantity' => [
                'relative' => true,
                'value' => 1,
            ]
        ]);

        return back()->with('message', 'Item Added Quantity');
    }

    public function decrease_item($rowId)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsUser->update($rowId, [
            'quantity' => [
                'relative' => true,
                'value' => -1,
            ]
        ]);

        return back()->with('message', 'Item Min Quantity');
    }

    public function remove_from_cart($rowId)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsUser->remove($rowId);

        return back()->with('message', 'Item Remove From Cart');
    }

    public function cart_reset()
    {
        $cartsUser = \Cart::session(auth()->user()->id)->clear();
        return back()->with('message', 'Item Reset From Cart');
    }
}
