<?php

namespace App\Http\Controllers;

use App\Models\ItemOrder;
use App\Models\Order;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function index()
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsCollect = $cartsUser->getContent();

        $subTotal = $cartsUser->getSubTotal();
        $total = $cartsUser->getTotal();

        $summary = [
            'subTotal' => $subTotal,
            'total' => $total,
        ];

        return view('checkout', compact('cartsCollect', 'summary'));
    }

    public function order(Request $request)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsCollect = $cartsUser->getContent();

        $total = session()->get('sum');

        $payment_select = $request->payment_method;

        if ($payment_select == 'cash') {
            $method = 'Cash';
        }
        elseif ($payment_select == 'bank') {
            $method = 'Bank';
        }
        elseif ($payment_select == 'card') {
            $method = 'Card';
        }

        $order = Order::create([
            'user_id' => auth()->user()->id ? auth()->user()->id : null,
            'name' => request('name'),
            'email' => auth()->user()->email,
            'phone' => request('phone'),
            'address' => request('address'),
            'city' => request('city'),
            'province' => request('province'),
            'payment_gateway' => $method,
            'total' => $total,
        ]);

        foreach($cartsCollect as $item){
            ItemOrder::create([
                'item_id' => $item->id,
                'order_id' => $order->id,
                'item_name' => $item->name,
                'price' => $item->price,
                'quantity' => $item->quantity,
            ]);
        }

        $cartsCollect = $cartsUser->clear();

        return redirect()->route('myorder');
    }
}
