<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function myorder()
    {
        $user = Auth::user()->id;
        $myOrder = Order::where('user_id', $user)->get();

        return view('myorder', compact('myOrder'));
    }

    public function detail(Order $myOrder)
    {
        return view('myorder-detail', compact('myOrder'));
    }
}
