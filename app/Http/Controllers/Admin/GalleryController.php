<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    public function manage()
    {
        $galleries = Gallery::all();
        return view('admin.gallery.manage', compact('galleries'));
    }

    public function add()
    {
        return view('admin.gallery.add-gallery');
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            $imageFile = $request->image;
            $galleryImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/api/images/galleries/', $galleryImage);
        }

        $gallery = Gallery::create([
            'title' => request('title'),
            'slug' => Str::slug(request('title')),
            'image' => $galleryImage,
        ]);

        return redirect()->route('gallery.manage')->with('message',''. $gallery->title.' Added Succesfully');
    }

    public function edit(Gallery $gallery)
    {
        return view('admin.gallery.edit-gallery', compact('gallery'));
    }

    public function update(Request $request, Gallery $gallery)
    {
        $galleryImage = $gallery->image;
        $pathImage = public_path(). '/api/images/galleries/'. $gallery->image;

        if ($request->hasFile('image')) {
            File::delete($pathImage);
            $imageFile = $request->image;
            $galleryImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/api/images/galleries/', $galleryImage);
        }
        elseif ($gallery->image) {
            $galleryImage = $gallery->image;
        }
        else {
            $galleryImage = null;
        }

        $gallery->update([
            'title' => request('title'),
            'slug' => Str::slug(request('title')),
            'image' => $galleryImage,
        ]);

        return redirect()->route('gallery.manage')->with('message',''. $gallery->title.' Update Succesfully');
    }

    public function delete(Gallery $gallery)
    {
        $galleryImage = public_path(). '/api/images/galleries/'. $gallery->image;
        File::delete($galleryImage);
        $gallery->delete();

        return back()->with('message', ''.$gallery->title. ' Deleted Succesfully');;
    }
}
