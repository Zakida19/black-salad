<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function manage()
    {
        $orders = Order::all();
        return view('admin.order.list', compact('orders'));
    }

    public function detail(Order $order)
    {
        return view('admin.order.order-detail', compact('order'));
    }

    public function edit_shipped(Order $order)
    {
        return view('admin.order.shipped', compact('order'));
    }

    public function shipped(Order $order)
    {
        $order->update([
            'shipped' => request()->input('shipped') ? true : false,
        ]);

        return redirect()->route('order.manage');
    }
}
