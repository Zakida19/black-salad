<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    public function manage()
    {
        $items = Item::latest()->get();
        return view('admin.item.manage', compact('items'));
    }

    public function create()
    {
        return view('admin.item.create-item');
    }

    public function detail(Item $item)
    {
        return view('admin.item.detail-item', compact('item'));
    }

    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $imageFile = $request->image;
            $itemImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/api/images/items/', $itemImage);
        }
        else {
            $itemImage = null;
        }

        $item = Item::create([
            'name' => request('name'),
            'slug' => Str::slug(request('name')),
            'image' => $itemImage,
            'price' => request('price'),
            'quantity' => request('quantity'),
            'description' => request('description'),
        ]);

        return redirect()->route('item.manage')->with('message', 'Item Created Succesfully');
    }

    public function edit(Item $item)
    {
        return view('admin.item.edit-item', compact('item'));
    }

    public function update(Item $item, Request $request)
    {
        $itemImage = $item->image;
        $pathImage = public_path(). '/api/images/items/'.$item->image;

        if ($request->hasFile('image')) {
            File::delete($pathImage);
            $imageFile = $request->image;
            $itemImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/api/images/items/', $itemImage);
        } elseif ($item->image) {
            $itemImage = $itemImage;
        }
        else {
            $itemImage = null;
        }

        $item->update([
            'name' => request('name'),
            'slug' => Str::slug(request('name')),
            'image' => $itemImage,
            'price' => request('price'),
            'quantity' => request('quantity'),
            'description' => request('description'),
            'isAvailable' => request()->input('isAvailable') ? true : false,
        ]);

        return redirect()->route('item.manage')->with('message', 'Item Updated Succesfully');
    }

    public function delete(Item $item)
    {
        $pathImage = public_path(). '/api/images/items/'.$item->image;
        File::delete($pathImage);
        $item->delete();

        return back()->with('message-danger', 'Item Deleted Succesfully');
    }
}
