<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\GalleryResource;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    public function manage()
    {
        $galleries = Gallery::all();
        $galleryResource = GalleryResource::collection($galleries);

        return $this->sendResponse($galleryResource, 'Get Galleries Successfully');
    }

    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'nullable|image|mimes:png,jpg,jpeg,gift',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validasi Tidak Sesuai', $validator->errors());
        }

        $imageFile = $request->image;
        $galleryImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
        $imageFile->move(public_path(). '/api/images/galleries/', $galleryImage);

        $gallery = Gallery::create([
            'title' => request('title'),
            'slug' => Str::slug(request('title')),
            'image' => $galleryImage,
        ]);

        return $this->sendResponse(new GalleryResource($gallery), 'Gallery Upload Successfully');
    }

    public function update(Gallery $gallery, Request $request)
    {
        $galleryImage = $gallery->image;
        $pathImage = public_path(). '/api/images/galleries/'. $gallery->image;

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:galleries,title,'. $gallery->id,
            'image' => 'nullable|image|mimes:png,jpg,jpeg,gift',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validasi Tidak Sesuai', $validator->errors());
        }

        if ($request->hasFile('image')) {
            File::delete($pathImage);
            $imageFile = $request->image;
            $galleryImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/api/images/galleries/', $galleryImage);
        }
        elseif ($gallery->image) {
            $galleryImage = $gallery->image;
        }
        else {
            $galleryImage = null;
        }

        $gallery->update([
            'title' => request('title'),
            'slug' => Str::slug(request('title')),
            'image' => $galleryImage,
        ]);

        return $this->sendResponse(new GalleryResource($gallery), 'Gallery Updated Successfully');
    }

    public function delete(Gallery $gallery)
    {
        $pathImage = public_path(). '/api/images/galleries/'. $gallery->image;
        File::delete($pathImage);
        $gallery->delete();

        return $this->sendResponse($gallery->title, 'Gallery Deleted Successfully');
    }
}
