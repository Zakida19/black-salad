<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemOrderResource;
use App\Http\Resources\OrderResource;
use App\Models\ItemOrder;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function list()
    {
        $orders = Order::all();
        $orderResource = OrderResource::collection($orders);

        return $this->sendResponse($orderResource, 'Get Orders Successfully');
    }

    public function detail(Order $order)
    {
        $itemOrder = ItemOrder::where('order_id', $order->id)->get();

        if (!empty($order)) {
            // return $this->sendResponse(new OrderResource($order), ItemOrderResource::collection($itemOrder),  'Get Order Detail');
            return response()->json([
                'success' => true,
                'message' => 'Get Order Detail',
                'data' => new OrderResource($order),
                'item' => ItemOrderResource::collection($itemOrder),
            ]);

        }
        else {
            return $this->sendError('Order Not Found');
        }
    }

    public function delete(Order $order)
    {
        $order->delete();
        return $this->sendResponse($order->name, 'Order Deleted Successfully!');
    }
}
