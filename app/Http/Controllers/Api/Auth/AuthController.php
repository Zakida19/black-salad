<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'confirm_password' => ['required', 'same:password'],
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        $success['name'] = $user->name;

        return $this->sendResponse($success, 'Register Success');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $authLogin = Auth::user();
            $success['token'] = $authLogin->createToken('auth_token')->plainTextToken;
            $success['name'] = $authLogin->name;

            return $this->sendResponse($success, 'Login Succesfully');
        }
        else {
            return $this->sendError('Email dan Password tidak sesuai');
        }
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'User Logout Successfully'
        ]);
    }
}
