<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemResource;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    public function manage()
    {
        $items = Item::all();
        $itemsResource = ItemResource::collection($items);

        return $this->sendResponse($itemsResource, 'Get Items Successfully!');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:items,name,',
            'image' => 'nullable|image|mimes:png,jpg,jpeg,gift',
            'price' => 'required',
            'quantity' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }

        $imageFile = $request->image;
        $itemImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
        $imageFile->move(public_path(). '/api/images/items/', $itemImage);

        // $input = $request->all();
        // $input['slug'] = Str::slug($input['name']);
        // $input['image'] = $itemImage;

        // $item = Item::create($input);
        $item = Item::create([
            'name' => request('name'),
            'slug' => Str::slug(request('name')),
            'image' => $itemImage,
            'price' => request('price'),
            'quantity' => request('quantity'),
            'description' => request('description'),
        ]);

        return $this->sendResponse(new ItemResource($item), 'Item Created Successfully!');
    }

    public function detail($id)
    {
        $item = Item::find($id);

        if (!empty($item)) {
            return $this->sendResponse(new ItemResource($item), 'Get Item ' .$item->name. ' Succesfully');
        }
        else {
            return $this->sendError('Item Not Found');
        }
    }

    public function update(Item $item, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:items,name,'. $item->id,
            'image' => 'nullable|image|mimes:png,jpg,jpeg,gift',
            'price' => 'required',
            'quantity' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Update Error', $validator->errors());
        }

        $itemImage = $item->image;
        $pathImage = public_path(). '/api/images/items/'.$item->image;

        if ($request->hasFile('image')) {
            File::delete($pathImage);
            $imageFile = $request->image;
            $itemImage = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/api/images/items', $itemImage);
        }
        elseif ($item->image) {
            $itemImage = $item->image;
        }
        else {
            $itemImage = null;
        }

        $item->update([
            'name' => request('name'),
            'slug' => Str::slug(request('name')),
            'image' => $itemImage,
            'price' => request('price'),
            'quantity' => request('quantity'),
            'description' => request('description'),
            'isAvailable' => request()->input('isAvailable') ? true : false,
        ]);

        return $this->sendResponse(new ItemResource($item), 'Item Updated Successfully!');
    }

    public function delete(Item $item)
    {
        $pathImage = public_path(). '/api/images/items/'.$item->image;

        File::delete($pathImage);
        $item->delete();

        return $this->sendResponse($item->name, 'Item Deleted Successfully!');
    }
}
