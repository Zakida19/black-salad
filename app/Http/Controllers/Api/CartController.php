<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartResource;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function mycart()
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsCollect = $cartsUser->getContent();

        $cartCollectResource = CartResource::collection($cartsCollect);

        return $this->sendResponse($cartCollectResource, 'Get Mycart Successfully!');
    }

    public function addToCart(Item $item)
    {
        $rowId = $item->id;

        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsCollect = $cartsUser->getContent();
        $cartsItems = $cartsCollect->whereIn('id', $rowId);

        if ($cartsItems->isNotEmpty()) {
            // Jika Cart Items yang ada di CartCollect tidak kosong/sudah ada maka tambhakna quantity nya
            $cartsUser->update($rowId, [
                'quantity' => [
                    'relative' => true,
                    'value' => 1,
                ]
            ]);
            return $this->sendResponse($cartsItems, 'Berhasil Menambahkan 1 Quantity Item ke Cart ');
        }
        else {
            // jika kosong/tidak ada
            $cartsUser->add([
                'id' => $item->id,
                'name' => $item->name,
                'price' => $item->price,
                'quantity' => 1,
                'attributes' => [
                    'image' => $item->image,
                    'add_to_cart' => Carbon::now()
                ],
            ]);

            return $this->sendResponse($cartsUser, 'Berhasil Menambahkan Item ke Cart ');
        }
    }

    public function increaseItem($rowId)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsUser->update($rowId, [
            'quantity' => [
                'relative' => true,
                'value' => 1
            ]
        ]);

        return $this->sendResponse($cartsUser, 'Berhasil Menambahkan 1 Quantity');
    }

    public function decreaseItem($rowId)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsUser->update($rowId, [
            'quantity' => [
                'relative' => true,
                'value' => -1,
            ]
        ]);

        return $this->sendResponse($cartsUser, 'Berhasil Mengurangi 1 Quantity');
    }

    public function removeItem($rowId)
    {
        $cartsUser = \Cart::session(auth()->user()->id);
        $cartsUser->remove($rowId);

        return $this->sendResponse($cartsUser, 'Berhasil Menghapus Semua Item Dari Cart');
    }
}
