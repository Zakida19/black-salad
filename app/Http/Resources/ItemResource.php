<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'item' => $this->name,
            // 'image' => resource_path(). '/' . $this->image,
            'image' => $this->image,
            'price' => $this->price,
            'description' => $this->description,
            'quantity' => $this->quantity,
            'isAvailable' => $this->isAvailable ? true : false,
        ];
    }
}
